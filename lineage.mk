# Release name
PRODUCT_RELEASE_NAME := GalaxyAce4Neo

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/samsung/vivalto3mve3gltn/device.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := vivalto3mve3gltn
PRODUCT_NAME := lineage_vivalto3mve3gltn
PRODUCT_BRAND := samsung
PRODUCT_MANUFACTURER := samsung
PRODUCT_MODEL := SM-G316ML
PRODUCT_CHARACTERISTICS := phone
